import com.ass.marsrover.model.Rover;
import com.ass.marsrover.operations.MoveRover;
import com.ass.marsrover.utils.Globals;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MoveRoverTest {
    MoveRover moveRover = new MoveRover();

    @Test
    public void testMoveRover() {
        System.out.println("test case move rover");
        String input = "1 2 N";
        String commands = "LMLMLMLMM";
        String input2 = "3 3 E";
        String commands2 = "MMRMMRMRRM";

        Rover rover = Globals.toRoverObject(input);
        rover.setCommands(commands);
        assertEquals("1 3 N", moveRover.moveRover(rover));
        rover = Globals.toRoverObject(input2);
        rover.setCommands(commands2);
        assertEquals("5 1 E", moveRover.moveRover(rover));
    }

    @Test
    public void testMoveRovers() {
        System.out.println("test case move rovers");
        String input = "5 5\n" +
                "\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "\n" +
                "3 3 E\n" +
                "MMRMMRMRRM\n" +
                "\n" +
                "2 3 E\n" +
                "MMRMMRMRRM";

        List<Rover> rover = Globals.toRoversObject(input);

        assertEquals("1 3 N" +
                "\n" +
                "5 1 E\n" +
                "4 1 E", moveRover.moveRover(rover));
    }
}
