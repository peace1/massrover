package com.ass.marsrover.operations;

import com.ass.marsrover.interfaces.OperationsImpl;
import com.ass.marsrover.model.Rover;

import java.util.List;

import static com.ass.marsrover.utils.Globals.*;


public class MoveRover implements OperationsImpl {

    private Rover rover = new Rover();

    private String processCommands(String commands) {
        for (Character singleCommand : commands.toCharArray()) {
            switch (singleCommand) {
                case COMMAND_L:
                    switchLeft(this.rover);
                    break;
                case COMMAND_R:
                    switchRight(this.rover);
                    break;
                case COMMAND_M:
                    move();
                    break;
                default:
                    throw new RuntimeException("I don't know that command");
            }
        }
        return displayRoverPosition(this.rover);
    }

    private void move() {
        int x = this.rover.position.getX(), y = this.rover.position.getY();
        switch (this.rover.cardinalPointer) {
            case N:
                this.rover.position.setY(++y);
                break;
            case E:
                this.rover.position.setX(++x);
                break;
            case S:
                this.rover.position.setY(--y);
                break;
            case W:
                this.rover.position.setX(--x);
                break;
        }
    }

    @Override
    public String moveRover(Rover rover) {
        this.rover = rover;
        String result = "";
        if (!this.rover.commands.isEmpty())
            result = processCommands(this.rover.commands);

        return result;
    }

    @Override
    public String moveRover(List<Rover> rovers) {
        String result = "";
        for (Rover rover : rovers) {
            result = result + moveRover(rover)+"\n";
        }
        return result.trim();
    }

    private void switchRight(Rover rover) {

        int changeOrientation = rover.cardinalPointer;
        if ((changeOrientation + 1) > W) {
            this.rover.setCardinalPointer(N);
        } else {
            this.rover.setCardinalPointer(changeOrientation + 1);
        }
    }

    private void switchLeft(Rover rover) {

        int changeOrientation = rover.cardinalPointer;
        if ((changeOrientation - 1) < N) {
            this.rover.setCardinalPointer(W);
        } else {
            this.rover.setCardinalPointer(changeOrientation - 1);
        }
    }

    private String displayRoverPosition(Rover rover) {
        System.out.println(rover.toString());
        return rover.toString();
    }
}
