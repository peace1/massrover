package com.ass.marsrover.interfaces;

import com.ass.marsrover.model.Rover;

import java.util.List;

public interface OperationsImpl {
    String moveRover(Rover rover);
//    String flyRover(Rover rover, double altitude);
    String moveRover(List<Rover> rovers);
}