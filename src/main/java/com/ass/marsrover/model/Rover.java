package com.ass.marsrover.model;


import static com.ass.marsrover.utils.Globals.*;

public class Rover {
    public Position position;
    public int cardinalPointer = N;
    public String commands = "";

    public Rover() {
    }

    public Rover(Position position) {
        this.position = position;
    }

    public Rover(Position position, int cardinalPointer) {
        this.position = position;
        this.cardinalPointer = cardinalPointer;
    }

    public void setCommands(String commands) {
        this.commands = commands;
    }

    public void setCardinalPointer(int cardinalPointer) {
        this.cardinalPointer = cardinalPointer;
    }

    public String toString() {
        return position.getX() + " " + position.getY() + " " + showDirection(cardinalPointer);
    }

    public char showDirection(int cardinalPointer) {
        switch (cardinalPointer) {
            case W:
                return WEST;
            case S:
                return SOUTH;
            case E:
                return EAST;
            default:
                return NORTH;
        }
    }
}
