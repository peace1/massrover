package com.ass.marsrover.utils;

import com.ass.marsrover.model.Position;
import com.ass.marsrover.model.Rover;

import java.util.ArrayList;
import java.util.List;

public class Globals {
    public static final char COMMAND_L = 'L';
    public static final char COMMAND_R = 'R';
    public static final char COMMAND_M = 'M';

    public static final int N = 1;
    public static final int E = 2;
    public static final int S = 3;
    public static final int W = 4;

    public static final char NORTH = 'N';
    public static final char SOUTH = 'S';
    public static final char EAST = 'E';
    public static final char WEST = 'W';

    public static List<Rover> toRoversObject(String input) {
        if (input == null) {
            throw new RuntimeException("Input not found");
        }
        if (input.isEmpty()) {
            throw new RuntimeException("Input not found");
        }
        String[] deployedRovers = null;
        if (input.contains("\n")) {
            deployedRovers = input.split("\n\n");
        }
        return buildDeployedRovers(deployedRovers);
    }

    private static List<Rover> buildDeployedRovers(String[] deployedRovers) {
        List<Rover> rovers = new ArrayList<>();
        if (deployedRovers != null) {
            for (String roverString : deployedRovers) {
                Rover rover = new Rover();
                if (roverString.contains("\n")) {
                    String[] roverData = roverString.split("\n");
                    rover = toRoverObject(roverData[0]);
                    rover.commands = roverData[1];
                } else {
                    rover.position = getPosition(roverString.split(" "));
                }

                rovers.add(rover);
            }
        }
        return rovers;
    }

    private static Position getPosition(String[] splitPosition) {
        Position pos = null;
        try {
            pos = new Position(Integer.parseInt(splitPosition[0]), Integer.parseInt(splitPosition[1]));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Invalid input format. Only digits allowed");
        }
        return pos;
    }

    public static Rover toRoverObject(String position) {
        if (position == null) {
            throw new RuntimeException("Input not found");
        }
        if (!position.contains(" ")) {
            throw new RuntimeException("Invalid input format");
        }
        String[] splitPosition = position.split(" ");
        if (splitPosition.length > 3) {
            throw new RuntimeException("Invalid input format > 3");
        }
        Position pos = getPosition(splitPosition);

        if (splitPosition.length > 2) {
            return new Rover(pos, getDirection(splitPosition[2].charAt(0)));
        } else {
            return new Rover(pos);
        }
    }

    public static int getDirection(Character cardinalPointer) {
        switch (cardinalPointer) {
            case NORTH:
                return N;
            case WEST:
                return W;
            case SOUTH:
                return S;
            case EAST:
                return E;
            default:
                return 0;
        }
    }
}
